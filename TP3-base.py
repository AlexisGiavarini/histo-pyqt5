import sys
import random
import pickle
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class myHisto(QLabel):

    def __init__(self, parent=None):
        QLabel.__init__(self, parent=parent)
        print('Constructeur de la classe myHisto')
        self.m_list = []

    #retourne la plus grande valeur contenue dans la liste
    def max(self):
        bin_max = 0
        for i in self.m_list:
            if i.m_amount > bin_max:
                bin_max = i.m_amount
        return bin_max

#Un Histogramme est constitué de plusieurs intervalles
class Intervalle:
    def __init__(self, a=0, b=0):
        print('Constructeur de la classe Intervalle')
        self.m_x = a
        self.m_amount = b


class MyMainWindow(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(300, 300, 600, 450)
        self.setWindowTitle('Main Window')
        # Barre de status pour afficher les infos
        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
        self.statusBar.showMessage("Zone d'informations, peut toujours servir")
        self.mHisto = myHisto()

        self.createActions()
        self.createMenus()

        self.setAcceptDrops(True) #Pour le drag and drop

        self.colorIcon = Qt.red         #variable membre rouge par défaut
        self.pMap = QPixmap(10,10)      #Pour contenir colorIcon
        self.pMap.fill(self.colorIcon)
        self.colorAct.setIcon(QIcon(self.pMap)) #Pour mettre l'icone rouge au demarrage du programme

        

    def createActions(self):
        print('Creation menu')
        # mise en relation article de menu -> slots **/
        self.openAct = QAction("&Open",self)
        self.openAct.setShortcut("Ctrl+O")
        self.saveAct = QAction("&Save",self)
        self.saveAct.setShortcut("Ctrl+S")
        self.restoreAct = QAction("&Restore",self)
        self.restoreAct.setShortcut("Ctrl+R")
        self.exitAct = QAction("&Bye...", self)
        self.exitAct.setShortcut("Ctrl+B")

        self.clearAct = QAction("&Clear",self)
        self.colorAct = QAction("&Color",self)

        self.exitAct.triggered.connect(self.exit)
        self.openAct.triggered.connect(self.open)
        self.saveAct.triggered.connect(self.save)
        self.clearAct.triggered.connect(self.clear)
        self.restoreAct.triggered.connect(self.restore)
        self.colorAct.triggered.connect(self.color)

    def createMenus(self):
        menuBar = self.menuBar()
        fileMenu = menuBar.addMenu("&File")      
        fileMenu.addAction(self.openAct)
        fileMenu.addAction(self.saveAct)
        fileMenu.addAction(self.restoreAct)
        fileMenu.addAction(self.exitAct)

        displayMenu = menuBar.addMenu("&Display")
        displayMenu.addAction(self.clearAct)
        displayMenu.addAction(self.colorAct)
  

    def open(self):
        fileName, ext = QFileDialog.getOpenFileName(self,'Open File',"/home/agiavari/Documents/S6/Evenementiel/tp3","*.dat")
        if fileName:
            self.clear()
            self.statusBar.showMessage("Histogram opened !")
            file = QFile(fileName)
            file.open(QIODevice.ReadOnly)

            #Chargement de l'histogramme
            if(len(self.mHisto.m_list) == 0):
                while not file.atEnd():
                    line = file.readLine()[:-2]
                    inter = Intervalle(int (line.split(' ')[0]),int(line.split(' ')[1]))

                    self.mHisto.m_list.append(inter)
                self.update()
        file.close()


    def paintEvent(self,event):
        #Dessiner l'histogramme
        qp = QPainter()
        qp.begin(self)

        if(len(self.mHisto.m_list) > 0):
            nb = len(self.mHisto.m_list)
            hmax = self.mHisto.max()
            ecart = 0

            painter = QPainter(self)
            pen = QPen(Qt.black, 2)
            pen.setStyle(Qt.SolidLine)
            painter.setPen(pen)
            painter.setBrush(QBrush(QColor(self.colorIcon)))


            for inter in self.mHisto.m_list:
                yMax = -inter.m_amount*(self.height()-self.menuBar().height())/hmax #La valeur max  = la hauteur
                yMin = self.height()-self.statusBar.height()

                painter.drawRect((ecart),yMin,(self.width()/nb),yMax)
                ecart += self.width()/(nb)
            
        qp.end()

    def color(self):
        col = QColorDialog.getColor()
        self.colorIcon = col
        self.pMap.fill(self.colorIcon)
        self.colorAct.setIcon(QIcon(self.pMap))

    def restore(self):
        self.mHisto.m_list = pickle.load(open('saveValues.bin', 'rb'))
        self.update()
        self.statusBar.showMessage("Histogram restored !")

    def save(self):
        pickle.dump(self.mHisto.m_list,open('saveValues.bin', 'wb'))
        self.statusBar.showMessage("Histogram saved !")

    def exit(self):
        self.statusBar.showMessage("Invoked File|Bye ...")
        QApplication.quit()

    def clear(self):
        self.mHisto.m_list.clear()
        self.update()
        self.statusBar.showMessage("Histogram cleared !")

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        self.clear()
        for url in event.mimeData().urls():
            filename = url.toLocalFile()
            print(filename)

            self.statusBar.showMessage("Histogram opened !")
            file = QFile(url.toLocalFile())
            file.open(QIODevice.ReadOnly)

            #Chargement de l'histogramme
            if(len(self.mHisto.m_list) == 0):
                self.update()
                while not file.atEnd():
                    line = file.readLine()[:-2]
                    inter = Intervalle(int (line.split(' ')[0]),int(line.split(' ')[1]))
                    self.mHisto.m_list.append(inter)
                self.__charge = True
                self.update()
        file.close()

    def keyPressEvent(self, event): 
        if event.key() == Qt.Key_R:
            self.aleatoire()

    def aleatoire(self):
        self.clear()
        i = 0
        while i < 10:
            inter = Intervalle(i,random.randrange(0, 100, 1))
            self.mHisto.m_list.append(inter)
            self.update()
            i += 1

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec_()
